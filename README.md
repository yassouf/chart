# Helm Repo

This is the source for my personal helm repo, hosted with gitlab pages.

```
helm repo add errm https://soufianem370.gitlab.io/chart
helm repo update
helm install --name myapp errm/prometheus --tiller-namespace axp --namespace axp
```